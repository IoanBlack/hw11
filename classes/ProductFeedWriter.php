<?php


class ProductFeedWriter
{
   protected $projects = [];

   public function addProject(Workable $project){
       $this->projects[] = $project;
   }

    public function write()
    {
        $response = "";
        foreach ($this->projects as $project){
            $response.= '<div>';
            $response.= '<ul>';
            $response.= '<li><strong>'.$project->getTitle().'</strong></li>';
            $response.= '<li>'.$project->getDescription().'</li>';
            $response.= '<li>'.$project->getPrice(). '$' . ' </li>';
            $response.= '</ul>';
            $response.= '</div>';

        }
        return $response;
    }
}