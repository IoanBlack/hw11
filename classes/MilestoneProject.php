<?php

require_once 'classes/Project.php';

class MilestoneProject extends Project
{
    protected $totalStagesAmount;
    protected $currentStage;
    protected $projectPrice;


    public function __construct($title,$description,$totalStagesAmount, $currentStage, $projectPrice)
    {
        parent::__construct($title,$description);
        $this->totalStagesAmount = $totalStagesAmount;
        $this->currentStage = $currentStage;
        $this->projectPrice = $projectPrice;
    }


    public function getPrice()
    {
        return $this->projectPrice;
    }

    public function getProjectProgress()
    {
        return $this->currentStage / $this->totalStagesAmount * 100;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getDescription()
    {
        return $this->description;
    }
}