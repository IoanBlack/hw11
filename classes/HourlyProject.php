<?php

require_once 'classes/Project.php';

class HourlyProject extends Project
{
    protected $totalHoursAmount;
    protected $currentSpentHours;
    protected $hourPrice;


    public function __construct($title,$description,$totalHoursAmount, $currentSpentHours, $hourPrice)
    {
        parent::__construct($title,$description);
        $this->totalHoursAmount = $totalHoursAmount;
        $this->currentSpentHours = $currentSpentHours;
        $this->hourPrice = $hourPrice;
    }


    public function getPrice()
    {
        $result = $this->totalHoursAmount * $this->hourPrice;
        return round($result,2);
    }

    public function getProjectProgress()
    {
        return $this->currentSpentHours /$this->totalHoursAmount * 100;

    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getDescription()
    {
        return $this->description;
    }
}