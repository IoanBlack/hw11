<?php

require_once 'interfaces/Workable.php';
require_once 'classes/HourlyProject.php';
require_once 'classes/MilestoneProject.php';
require_once 'classes/Task.php';
require_once 'classes/ProductFeedWriter.php';
require_once 'arrays.php';

$writer = new ProductFeedWriter();

foreach ($hourlyObjs as $hourly) {
    $writer->addProject($hourly);
}

foreach ($stageObjs as $stage) {
    $writer->addProject($stage);
}

foreach ($taskObjs as $task) {
    $writer->addProject($task);
}

?><!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Projects</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
          integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Projects</h1>
                <?=$writer->write();?>
            </div>
        </div>
    </div>
</body>
</html>