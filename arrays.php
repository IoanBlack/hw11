<?php

require_once 'classes/HourlyProject.php';
require_once 'classes/MilestoneProject.php';
require_once 'classes/Task.php';
require_once 'classes/ProductFeedWriter.php';

$hourlyObjs = [];
$stageObjs = [];
$taskObjs = [];

$hourlyObjs[] = $hourlyProject = new HourlyProject('Pochasoviy','Lorem ipsum dolor sit amet, consectetur adipisicing elit.
    Aperiam aspernatur eum inventore natus ratione! Accusantium aliquid 
    amet atque consequatur distinctio ducimus, eaque eos et ex excepturi 
    exercitationem explicabo libero magnam magni natus, nemo neque quam 
    quibusdam quod recusandae reprehenderit similique suscipit tenetur. 
    Ab commodi, consectetur consequuntur deserunt distinctio dolor 
    dolorum facere inventore libero mollitia nulla omnis perspiciatis
    praesentium quae quia quibusdam sapiente totam unde vel voluptates? 
    Alias neque quae temporibus?',127,33,12);

$hourlyObjs[] = $pochasoviyProekt = new HourlyProject('Hourly','Lorem ipsum dolor sit amet, consectetur adipisicing elit.
    Aperiam aspernatur eum inventore natus ratione! Accusantium aliquid 
    amet atque consequatur distinctio ducimus, eaque eos et ex excepturi 
    exercitationem explicabo libero magnam magni natus, nemo neque quam 
    quibusdam quod recusandae reprehenderit similique suscipit tenetur. 
    Ab commodi, consectetur consequuntur deserunt distinctio dolor 
    dolorum facere inventore libero mollitia nulla omnis perspiciatis
    praesentium quae quia quibusdam sapiente totam unde vel voluptates? 
    Alias neque quae temporibus?',90,68,14);

$hourlyObjs[] = $xProject = new HourlyProject('Pogodynniy','Lorem ipsum dolor sit amet, consectetur adipisicing elit.
    Aperiam aspernatur eum inventore natus ratione! Accusantium aliquid 
    amet atque consequatur distinctio ducimus, eaque eos et ex excepturi 
    exercitationem explicabo libero magnam magni natus, nemo neque quam 
    quibusdam quod recusandae reprehenderit similique suscipit tenetur. 
    Ab commodi, consectetur consequuntur deserunt distinctio dolor 
    dolorum facere inventore libero mollitia nulla omnis perspiciatis
    praesentium quae quia quibusdam sapiente totam unde vel voluptates? 
    Alias neque quae temporibus?',144,41,10);

$stageObjs[] = $mileStoneProject = new MilestoneProject('Poetapniy','Lorem ipsum dolor sit amet, consectetur
    adipisicing elit. Ab animi atque beatae consequatur deserunt distinctio ea eaque 
    error eum exercitationem fuga illum laborum molestiae neque, nobis non omnis quas 
    reiciendis sapiente similique sint tenetur vitae voluptates? A accusamus accusantium
    animi assumenda consequuntur cumque deserunt, distinctio doloremque dolores ducimus 
    eligendi, expedita fugiat incidunt ipsam maiores molestias officiis optio quam quis voluptatem.',12,7,1700);

$stageObjs[] = $stage = new MilestoneProject('Stage','Lorem ipsum dolor sit amet, consectetur
    adipisicing elit. Ab animi atque beatae consequatur deserunt distinctio ea eaque 
    error eum exercitationem fuga illum laborum molestiae neque, nobis non omnis quas 
    reiciendis sapiente similique sint tenetur vitae voluptates? A accusamus accusantium
    animi assumenda consequuntur cumque deserunt, distinctio doloremque dolores ducimus 
    eligendi, expedita fugiat incidunt ipsam maiores molestias officiis optio quam quis voluptatem.',10,4,2000);

$stageObjs[] = $prostoProject = new MilestoneProject('Project','Lorem ipsum dolor sit amet, consectetur
    adipisicing elit. Ab animi atque beatae consequatur deserunt distinctio ea eaque 
    error eum exercitationem fuga illum laborum molestiae neque, nobis non omnis quas 
    reiciendis sapiente similique sint tenetur vitae voluptates? A accusamus accusantium
    animi assumenda consequuntur cumque deserunt, distinctio doloremque dolores ducimus 
    eligendi, expedita fugiat incidunt ipsam maiores molestias officiis optio quam quis voluptatem.',8,6,1200);

$taskObjs[] = $task = new Task('Zadacha','Lorem ipsum dolor sit amet, consectetur adipisicing elit.
     Alias amet animi assumenda consequatur deserunt ea, excepturi explicabo id illo laboriosam minima 
     necessitatibus ratione rerum saepe sed. A adipisci cumque distinctio dolores eum exercitationem
     in non recusandae rerum voluptas! Beatae consequuntur doloremque harum illo in ipsum nisi odit,
     officia, omnis quasi reiciendis, rem sapiente sit vitae.',800);

$taskObjs[] = $mission = new Task('Mission','Lorem ipsum dolor sit amet, consectetur adipisicing elit.
     Alias amet animi assumenda consequatur deserunt ea, excepturi explicabo id illo laboriosam minima 
     necessitatibus ratione rerum saepe sed. A adipisci cumque distinctio dolores eum exercitationem
     in non recusandae rerum voluptas! Beatae consequuntur doloremque harum illo in ipsum nisi odit,
     officia, omnis quasi reiciendis, rem sapiente sit vitae.',700);

$taskObjs[] = $greatTask = new Task('Great Task','Lorem ipsum dolor sit amet, consectetur adipisicing elit.
     Alias amet animi assumenda consequatur deserunt ea, excepturi explicabo id illo laboriosam minima 
     necessitatibus ratione rerum saepe sed. A adipisci cumque distinctio dolores eum exercitationem
     in non recusandae rerum voluptas! Beatae consequuntur doloremque harum illo in ipsum nisi odit,
     officia, omnis quasi reiciendis, rem sapiente sit vitae.',1000);